<?php

namespace ReservationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class reservationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateReservation', 'hidden')

            ->add('heureDebut', 'text',
                [
                    'attr' => [
                        'class' => 'form-control input-inline timepicker',
                        'placeholder'=> 'Choisir une heure debut'
                    ], 'required' => false])
            ->add('heureFin', 'text',
                [
                    'attr' => [
                        'class' => 'form-control input-inline timepicker',
                        'placeholder'=> 'Choisir une heure fin'
                    ], 'required' => false])
            ->add('email')
            ->add('prix', 'hidden')
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ReservationBundle\Entity\reservation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'reservationbundle_reservation';
    }


}
