<?php

namespace ReservationBundle\Controller;

use ReservationBundle\Entity\reservation;
use ReservationBundle\Form\reservationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Session\Session;
use DoctrineExtensions\Query\Mysql;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ReservationBundle:Default:index.html.twig');
    }
    public function neweventAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $startDateField = $request->request->get('startdate');
        $endDateField = $request->request->get('enddate');


        $conn->insert('agenda', array('date_debut' => $startDateField, 'date_fin' => $endDateField));

        return $this->render('ReservationBundle:Default:event.html.twig');
    }

    public function getalleventsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $rows = array();

        $query = $em->createQuery("SELECT a.id, a.dateDebut, a.dateFin FROM ReservationBundle:agenda a");
        $result = $query->getResult();

        foreach ($result as $r) {
            $row['id'] = $r['id'];
            $row['start'] = $r['dateDebut'];
            $row['end'] = $r['dateFin'];
            $row['title'] = '';

            array_push($rows,$row);

        }
        $response = new JsonResponse();
        $response->setEncodingOptions(JSON_NUMERIC_CHECK);
        $response->setData($rows);
        return $response;
    }

    public function reservationcoursAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new reservation();
        $form = $this->createForm(new reservationType(), $entity);

        $query = $em->createQuery("SELECT a.id, DATE_FORMAT(a.dateDebut, '%Y-%m-%d %H:%i:%s') as dateDebut , DATE_FORMAT(a.dateFin, '%H:%i:%s') as dateFin FROM ReservationBundle:agenda a");
        $datebebutplanning = $query->getResult();

        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('notice', 'Vous venez de faire une reservation avec l\'email ' .$entity->getEmail().' au prix de '.$entity->getPrix());
            }
        }

        $reservation = $em->getRepository('ReservationBundle:reservation')->findAll();

        return $this->render('ReservationBundle:Default:reservation.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'datebebutplanning' => $datebebutplanning,
            'reservation' => $reservation
        ));
    }

    public function getinfoplanningAction(Request $request)
    {
        {
            $em = $this->getDoctrine()->getManager();

            $idplanning = $request->query->get('idplanning');

            $query = $em->createQuery("SELECT a.id, DATE_FORMAT(a.dateDebut, '%Y-%m-%d') as datep,DATE_FORMAT(a.dateDebut, '%H:%i:%s') as heuredebut,DATE_FORMAT(a.dateFin, '%H:%i:%s') as heurefin  FROM ReservationBundle:agenda a WHERE a.id = $idplanning");
            $result = $query->getResult();


            return new JsonResponse($result);
        }
    }
}
