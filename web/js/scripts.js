/**
 * Created by akdaho on 03/12/2016.
 */

//Implementation du pluing jquery week calendar pour le planning du professeur

$(document).ready(function() {


    var $calendar = $('#calendar');
    var id = 10;

    $calendar.weekCalendar({
        timeslotsPerHour : 4,
        allowCalEventOverlap : true,
        overlapEventsSeparate: true,
        firstDayOfWeek : 1,
        businessHours :{start: 6, end: 22, limitDisplay: true },
        daysToShow : 7,
        height : function($calendar) {
            return $(window).height() - $("h1").outerHeight() - 1;
        },
        eventRender : function(calEvent, $event) {
            if (calEvent.end.getTime() < new Date().getTime()) {
                $event.css("backgroundColor", "#aaa");
                $event.find(".wc-time").css({
                    "backgroundColor" : "#999",
                    "border" : "1px solid #888"
                });
            }
        },
        draggable : function(calEvent, $event) {
            return calEvent.readOnly != true;
        },
        resizable : function(calEvent, $event) {
            return calEvent.readOnly != true;
        },
        eventNew : function(calEvent, $event) {
            var $dialogContent = $("#event_edit_container");
            resetForm($dialogContent);
            var startField = $dialogContent.find("select[name='start1']").val(calEvent.start);
            var endField = $dialogContent.find("select[name='end1']").val(calEvent.end);


            $dialogContent.dialog({
                modal: true,
                title: "Nouveau planning",
                close: function() {
                    $dialogContent.dialog("destroy");
                    $dialogContent.hide();
                    $('#calendar').weekCalendar("removeUnsavedEvents");
                },
                buttons: {
                    save : function() {
                        calEvent.id = id;
                        id++;
                        calEvent.start = new Date(startField.val());
                        calEvent.end = new Date(endField.val());

                        // Ajout d'un nouveau evennement

                        var datas = "startdate="+ calEvent.start.toISOString() + "&enddate="+calEvent.end.toISOString();
                        $.ajax({
                            type: 'POST',
                            url: Routing.generate('newevent'),
                            data: datas,
                            dataType: 'html',
                            success: function (data) {
                                location.reload(true);
                            }
                        });

                        $calendar.weekCalendar("removeUnsavedEvents");
                        $calendar.weekCalendar("updateEvent", calEvent);
                        $dialogContent.dialog("close");
                    },
                    cancel : function() {
                        $dialogContent.dialog("close");
                    }
                }
            }).show();

            $dialogContent.find(".date_holder").text($calendar.weekCalendar("formatDate", calEvent.start));
            setupStartAndEndTimeFields(startField, endField, calEvent, $calendar.weekCalendar("getTimeslotTimes", calEvent.start));

        },
        eventDrop : function(calEvent, $event) {

        },
        eventResize : function(calEvent, $event) {
        },
        eventClick : function(calEvent, $event) {

            if (calEvent.readOnly) {
                return;
            }

            var $dialogContent = $("#event_edit_container");
            resetForm($dialogContent);
            var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
            var endField = $dialogContent.find("select[name='end']").val(calEvent.end);

            $dialogContent.dialog({
                modal: true,
                title: "Modifier planning",
                close: function() {
                    $dialogContent.dialog("destroy");
                    $dialogContent.hide();
                    $('#calendar').weekCalendar("removeUnsavedEvents");
                },
                buttons: {
                    save : function() {

                        calEvent.start = new Date(startField.val());
                        calEvent.end = new Date(endField.val());

                        // Modification planning

                        var datas = "startdate="+ calEvent.start.toISOString()+ "&enddate="+calEvent.end.toISOString()+ "&id="+calEvent.id;
                        $.ajax({
                            type: 'POST',
                            url: Routing.generate('editevent'),
                            data: datas,
                            dataType: 'html',
                            success: function (data) {
                                location.reload(true);
                            }
                        });

                        $calendar.weekCalendar("updateEvent", calEvent);
                        $dialogContent.dialog("close");
                    },
                    "delete" : function() {
                        //suppression planning
                        var datas = "id="+calEvent.id;
                        $.ajax({
                            type: 'POST',
                            url: Routing.generate('deleteevent'),
                            data: datas,
                            dataType: 'html',
                            success: function (data) {
                                location.reload(true);
                            }
                        });

                        $calendar.weekCalendar("removeEvent", calEvent.id);
                        $dialogContent.dialog("close");
                    },
                    cancel : function() {
                        $dialogContent.dialog("close");
                    }
                }
            }).show();

            var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
            var endField = $dialogContent.find("select[name='end']").val(calEvent.end);
            $dialogContent.find(".date_holder").text($calendar.weekCalendar("formatDate", calEvent.start));
            setupStartAndEndTimeFields(startField, endField, calEvent, $calendar.weekCalendar("getTimeslotTimes", calEvent.start));
            $(window).resize().resize(); //fixes a bug in modal overlay size ??

        },
        eventMouseover : function(calEvent, $event) {
        },
        eventMouseout : function(calEvent, $event) {
        },
        noEvents : function() {

        },
        data : Routing.generate('getallevents'),

    });

    function resetForm($dialogContent) {
        $dialogContent.find("input").val("");
        $dialogContent.find("textarea").val("");
    }


    /*
     * Sets up the start and end time fields in the calendar event
     * form for editing based on the calendar event being edited
     */
    function setupStartAndEndTimeFields($startTimeField, $endTimeField, calEvent, timeslotTimes) {

        for (var i = 0; i < timeslotTimes.length; i++) {
            var startTime = timeslotTimes[i].start;
            var endTime = timeslotTimes[i].end;
            var startSelected = "";
            if (startTime.getTime() === calEvent.start.getTime()) {
                startSelected = "selected=\"selected\"";
            }
            var endSelected = "";
            if (endTime.getTime() === calEvent.end.getTime()) {
                endSelected = "selected=\"selected\"";
            }
            $startTimeField.append("<option value=\"" + startTime + "\" " + startSelected + ">" + timeslotTimes[i].startFormatted + "</option>");
            $endTimeField.append("<option value=\"" + endTime + "\" " + endSelected + ">" + timeslotTimes[i].endFormatted + "</option>");

        }
        $endTimeOptions = $endTimeField.find("option");
        $startTimeField.trigger("change");
    }

    var $endTimeField = $("select[name='end']");
    var $endTimeOptions = $endTimeField.find("option");

    //reduces the end time options to be only after the start time options.
    $("select[name='start']").change(function() {
        var startTime = $(this).find(":selected").val();
        var currentEndTime = $endTimeField.find("option:selected").val();
        $endTimeField.html(
            $endTimeOptions.filter(function() {
                return startTime < $(this).val();
            })
        );

        var endTimeSelected = false;
        $endTimeField.find("option").each(function() {
            if ($(this).val() === currentEndTime) {
                $(this).attr("selected", "selected");
                endTimeSelected = true;
                return false;
            }
        });

        if (!endTimeSelected) {
            //automatically select an end date 2 slots away.
            $endTimeField.find("option:eq(1)").attr("selected", "selected");
        }

    });

});

$(document).ready(function() {


    var $calendar = $('#calendar1');
    var id = 10;

    $calendar.weekCalendar({
        timeslotsPerHour : 4,
        allowCalEventOverlap : true,
        overlapEventsSeparate: true,
        firstDayOfWeek : 1,
        businessHours :{start: 6, end: 22, limitDisplay: true },
        daysToShow : 7,
        height : function($calendar) {
            return $(window).height() - $("h1").outerHeight() - 1;
        },
        eventRender : function(calEvent, $event) {
            if (calEvent.end.getTime() < new Date().getTime()) {
                $event.css("backgroundColor", "#aaa");
                $event.find(".wc-time").css({
                    "backgroundColor" : "#999",
                    "border" : "1px solid #888"
                });
            }
        },
        draggable : function(calEvent, $event) {
            return calEvent.readOnly != true;
        },
        resizable : function(calEvent, $event) {
            return calEvent.readOnly != true;
        },
        eventDrop : function(calEvent, $event) {

        },
        eventResize : function(calEvent, $event) {
        },
        eventClick : function(calEvent, $event) {

            if (calEvent.readOnly) {
                return;
            }

            var $dialogContent = $("#event_edit_container");
            resetForm($dialogContent);
            var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
            var endField = $dialogContent.find("select[name='end']").val(calEvent.end);

            $dialogContent.dialog({
                modal: true,
                title: "Modifier planning",
                close: function() {
                    $dialogContent.dialog("destroy");
                    $dialogContent.hide();
                    $('#calendar').weekCalendar("removeUnsavedEvents");
                },
                buttons: {
                    save : function() {

                        calEvent.start = new Date(startField.val());
                        calEvent.end = new Date(endField.val());

                        // Modification planning

                        var datas = "startdate="+ calEvent.start.toISOString()+ "&enddate="+calEvent.end.toISOString()+ "&id="+calEvent.id;
                        $.ajax({
                            type: 'POST',
                            url: Routing.generate('editevent'),
                            data: datas,
                            dataType: 'html',
                            success: function (data) {
                                location.reload(true);
                            }
                        });

                        $calendar.weekCalendar("updateEvent", calEvent);
                        $dialogContent.dialog("close");
                    },
                    "delete" : function() {
                        //suppression planning
                        var datas = "id="+calEvent.id;
                        $.ajax({
                            type: 'POST',
                            url: Routing.generate('deleteevent'),
                            data: datas,
                            dataType: 'html',
                            success: function (data) {
                                location.reload(true);
                            }
                        });

                        $calendar.weekCalendar("removeEvent", calEvent.id);
                        $dialogContent.dialog("close");
                    },
                    cancel : function() {
                        $dialogContent.dialog("close");
                    }
                }
            }).show();

            var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
            var endField = $dialogContent.find("select[name='end']").val(calEvent.end);
            $dialogContent.find(".date_holder").text($calendar.weekCalendar("formatDate", calEvent.start));
            setupStartAndEndTimeFields(startField, endField, calEvent, $calendar.weekCalendar("getTimeslotTimes", calEvent.start));
            $(window).resize().resize(); //fixes a bug in modal overlay size ??

        },
        eventMouseover : function(calEvent, $event) {
        },
        eventMouseout : function(calEvent, $event) {
        },
        noEvents : function() {

        },
        data : Routing.generate('getallevents'),

    });

    function resetForm($dialogContent) {
        $dialogContent.find("input").val("");
        $dialogContent.find("textarea").val("");
    }


    /*
     * Sets up the start and end time fields in the calendar event
     * form for editing based on the calendar event being edited
     */
    function setupStartAndEndTimeFields($startTimeField, $endTimeField, calEvent, timeslotTimes) {

        for (var i = 0; i < timeslotTimes.length; i++) {
            var startTime = timeslotTimes[i].start;
            var endTime = timeslotTimes[i].end;
            var startSelected = "";
            if (startTime.getTime() === calEvent.start.getTime()) {
                startSelected = "selected=\"selected\"";
            }
            var endSelected = "";
            if (endTime.getTime() === calEvent.end.getTime()) {
                endSelected = "selected=\"selected\"";
            }
            $startTimeField.append("<option value=\"" + startTime + "\" " + startSelected + ">" + timeslotTimes[i].startFormatted + "</option>");
            $endTimeField.append("<option value=\"" + endTime + "\" " + endSelected + ">" + timeslotTimes[i].endFormatted + "</option>");

        }
        $endTimeOptions = $endTimeField.find("option");
        $startTimeField.trigger("change");
    }

    var $endTimeField = $("select[name='end']");
    var $endTimeOptions = $endTimeField.find("option");

    //reduces the end time options to be only after the start time options.
    $("select[name='start']").change(function() {
        var startTime = $(this).find(":selected").val();
        var currentEndTime = $endTimeField.find("option:selected").val();
        $endTimeField.html(
            $endTimeOptions.filter(function() {
                return startTime < $(this).val();
            })
        );

        var endTimeSelected = false;
        $endTimeField.find("option").each(function() {
            if ($(this).val() === currentEndTime) {
                $(this).attr("selected", "selected");
                endTimeSelected = true;
                return false;
            }
        });

        if (!endTimeSelected) {
            //automatically select an end date 2 slots away.
            $endTimeField.find("option:eq(1)").attr("selected", "selected");
        }

    });

});

//Heure debut reservation

$(document).ready(function(){
    $('#reservationbundle_reservation_heureDebut').timepicker({
        timeFormat: 'HH:mm:ss',
        interval: 15,
        'minTime': '00',
        'maxTime': '23:45',
        dynamic: false,
        dropdown: true,
        scrollbar: true,
        change: function(time) {
            $("#time").val($(this).val());
            var heurechoisi = new Date($("#date").val()+" "+$("#time").val()).getTime();
            var heuredebut = new Date($("#date").val()+" "+$("#mintime").val()).getTime();

            if(heurechoisi < heuredebut)
            {
                var heuredebut1 = new Date($("#date").val()+" "+$("#mintime").val()).toLocaleTimeString();
                alert("L'heure des debut des cours est " +heuredebut1+" choisisez une autre heure");

                $('#reservationbundle_reservation_heureDebut').val(heuredebut1);
            }
            else
            {

            }
        }

    })

    $('#reservationbundle_reservation_heureFin').timepicker({
        timeFormat: 'HH:mm:ss',
        interval: 15,
        'minTime': '00',
        'maxTime': '23:45',
        scrollbar: true,
        dynamic: false,
        dropdown: true,
        change: function(time) {
            $("#time").val($(this).val());
            var heurefinchoisi = new Date($("#date").val()+" "+$("#time").val()).getTime();
            var heurefin = new Date($("#date").val()+" "+$("#maxtime").val()).getTime();

            if(heurefinchoisi > heurefin)
            {
                var heurefin1 = new Date($("#date").val()+" "+$("#maxtime").val()).toLocaleTimeString();
                alert("L'heure de fin des cours est " +heurefin1+" choisisez une autre heure");
                $('#reservationbundle_reservation_heureFin').val(heurefin1);
            }
            else
            {
                var dateheuredebut = new Date($("#date").val()+" "+$("#reservationbundle_reservation_heureDebut").val()).getTime();
                var dateheurefin = new Date($("#date").val()+" "+$("#reservationbundle_reservation_heureFin").val()).getTime();
                var minutes = 1000 * 60;

                var nbminute = (dateheurefin / minutes) -  (dateheuredebut / minutes)

                $("#reservationbundle_reservation_prix").val((nbminute / 15) * 1000);
            }
        }

    })

});


$(function(){
    $("#reservationbundle_reservation_email").change(function(){
        var dateheuredebut = new Date($("#date").val()+" "+$("#reservationbundle_reservation_heureDebut").val()).getTime();
        var dateheurefin = new Date($("#date").val()+" "+$("#reservationbundle_reservation_heureFin").val()).getTime();
        var minutes = 1000 * 60;

        var nbminute = (dateheurefin / minutes) -  (dateheuredebut / minutes);

        $("#reservationbundle_reservation_prix").val((nbminute / 15) * 1000);
    });

    var datedebutp = $("#datedebutp");
    $(datedebutp).change(function(){
        $("#reservationbundle_reservation_dateReservation").val($(datedebutp).val());
        getinfoplanning($(datedebutp).val());

    });

});

function getinfoplanning(str)
{
    var id = str;
    var datas = "idplanning="+str;

    var minttime = $("#mintime");
    var maxtime = $("#maxtime");
    var date = $("#date");

    $.ajax({
        url:  Routing.generate('getinfoplanning'),
        type: "GET",
        data: datas,
        context: this,
        error: function () {},
        dataType: 'text',
        success : function (data) {
            var json_obj = $.parseJSON(data);//parse JSON
            console.log(data);

            $(minttime).val(json_obj[0].heuredebut);
            $(maxtime).val(json_obj[0].heurefin);
            $(date).val(json_obj[0].datep);

        }
    });
}
